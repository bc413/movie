import { message } from "antd";
import { useNavigate } from "react-router-dom";
import { localUserServ } from "../../service/localService";

import { useServ } from "../../service/userService";
import { SET_USER_LOGIN } from "../constant/userConstant";

export const setLoginAction = (values) => {
  return {
    type: SET_USER_LOGIN,
    payload: values,
  };
};

export const setLoginActionService = (values, onSuccess) => {
  return (dispatch) => {
    useServ
      .login(values)
      .then((res) => {
        console.log(res);
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        localUserServ.set(res.data.content);
        onSuccess();
      })
      .catch((err) => {
        message.error("Tài khoản hoặc mật khẩu sai");
        console.log(err);
      });
  };
};

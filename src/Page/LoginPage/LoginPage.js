import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { SET_USER_LOGIN } from "../../redux/constant/userConstant";
import { localUserServ } from "../../service/localService";
import { useServ } from "../../service/userService";
import Lottie from "lottie-react";
import login_Anime from "../../../src/asset/24722-pomeranian-dog.json";
import {
  setLoginAction,
  setLoginActionService,
} from "../../redux/action/useAction";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    useServ
      .login(values)
      .then((res) => {
        message.success("Đăng nhập thành công!");
        // dispatch({
        //   type: SET_USER_LOGIN,
        //   payload: res.data.content,
        // });
        dispatch(setLoginAction(res.data.content));
        localUserServ.set(res.data.content);
        // Chuyển hướng use về trang home
        navigate("/");
        console.log(res);
      })
      .catch((err) => {
        message.error("Tài khoản hoặc mật khẩu sai");
        console.loh(err);
      });
    console.log("Success:", values);
  };
  const onFinishThunk = (values) => {
    let hendleSuccess = () => {
      message.success("Đăng nhập thành công!");
      navigate("/");
    };
    dispatch(setLoginActionService(values, hendleSuccess));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="w-screen h-screen p-20 bg-orange-400 flex justify-center items-center">
      <div className="container p-20 bg-white rounded-lg flex">
        <div className="w-1/2 h-full ">
          <Lottie animationData={login_Anime} loop={true} />
        </div>
        <div className="w-1/2 h-full ">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="w-full"
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 24,
              }}
            >
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;

import React from "react";
import Header from "../../Components/Header/Header";
import ListMovie from "./ListMovie/ListMovie";
import TabsMovive from "./TabsMovive/TabsMovive";

export default function HomePage() {
  return (
    <div className="space-y-10">
      {/* <Header /> */}
      <ListMovie />
      <TabsMovive />
    </div>
  );
}

import React, { useEffect, useState } from "react";
import { movieServ } from "../../../service/movieService";
import ItemMovie from "./ItemMovie";

export default function ListMovie() {
  const [movieList, setMovieList] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        setMovieList(res.data.content);
        console.log(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className=" container grid grid-cols-6 gap-6">
      {movieList.slice(0, 8).map((item) => {
        return <ItemMovie data={item} key={item.maPhim} />;
      })}
    </div>
  );
}

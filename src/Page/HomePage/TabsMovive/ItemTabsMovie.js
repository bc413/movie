import moment from "moment/moment";
import React from "react";

export default function ItemTabsMovie({ phim }) {
  return (
    <div className=" flex space-x-5 items-center">
      <img src={phim.hinhAnh} className="w-28 h-48 object-cover" alt="" />
      <div>
        <h5 className="font-medium text-xl mb-5">{phim.tenPhim}</h5>
        <div className="grid gap-5 grid-cols-3">
          {phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
              <span className="rounded p-3 bg-red-400 text-white">
                {moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ~ hh:mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}

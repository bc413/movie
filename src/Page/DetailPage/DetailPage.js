import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../service/movieService";
import { Progress, Space } from "antd";

export default function DetailPage() {
  let params = useParams();
  let [movie, setMovie] = useState({});

  console.log(params);
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getDetailMovie(params.id);
        setMovie(result.data.content);
        console.log(result);
      } catch (error) {}
    };
    fetchDetail();
  }, []);
  return (
    <div className="container flex">
      <img src={movie.hinhAnh} alt="" />
      <div className="p-5">
        <h2 className="text-xl">{movie.tenPhim}</h2>
        <p className="text-xs text-gray-600"> {movie.moTa}</p>
        <Progress
          strokeColor={{
            "0%": "#108ee9",
            "100%": "#87d068",
          }}
          type="circle"
          percent={movie.danhGia * 10}
          format={(percent) => `${percent / 10} Điểm`}
        />
        <NavLink
          to={`/booking/${movie.maPhim}`}
          className="px-5 py-2 bg-orange-500 text-white"
        >
          Mua vé
        </NavLink>
      </div>
    </div>
  );
}

// progress antd

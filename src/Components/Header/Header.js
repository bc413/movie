import React from "react";
import { Desktop, Mobile, Tablet } from "../../Layout/Reponsice";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";
import HeaderTable from "./HeaderTable";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDesktop />
      </Desktop>
      <Tablet>
        <HeaderTable />
      </Tablet>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </div>
  );
}

import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const useServ = {
  login: (loginData) => {
    console.log("yes");
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: loginData,
      headers: configHeaders(),
    });
  },
};
